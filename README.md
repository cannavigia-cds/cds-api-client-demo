# CDS API PHP Client Demo

This is a simple demo on how to use the CDS API in PHP.

## Requirements

- PHP >= 7.4
- Apache or Nginx Webserver with PHP Support (mod_php, FPM or FCGI)
- Composer >= 2.0 (https://getcomposer.org/)

## Installation

### Step 1

Clone the repository and change to the directory.

```
git clone git@gitlab.com:cannavigia-cds/cds-api-client-demo.git
```

```
cd cds-api-client-demo
```

### Step 2

Install vendor packages.

```
composer install
```

### Step 3

Copy `.env.example` to `.env`. Create an API Client in your account and copy and paste the required info into your `.env`.

The `API_REDIRECT_URL` must be your base URL + `/oauth.php`, e.g. https://example.org/oauth.php.

```
cp .env.example .env
```

### Step 4

Point your webserver's document root to the `/public` directory of the installation.

#### Apache

```
DocumentRoot /path/to/cds-api-client-demo/public
```

#### Nginx

```
root /path/to/cds-api-client-demo/public;
```

## Authorization

Open `oauth.php` in your browser, e.g. http://example.org/oauth.php.

This should redirect you to the authorization page on the CDS. There click the "Authorize" button and you should be redirected back to the configured redirect URL with the URL parameter `code` and see a list with the returned access token data.

For convenience the access token is stored in your session and will be used to authorize the following request examples. If your session or authorization token expires, you will have to repeat this step.

**In your real world application the response JSON data containing the access token, its expiry and the refresh token should be stored in a database or file and used to authorize requests and refresh the access token after it expired.**

### Refresh Token

If your access token expired, you can refresh it by copying the obtained refresh token into `public/refresh-token.php` and opening `refresh-token.php` in your browser, e.g. http://example.org/refresh-token.php.

### Documentation

https://cds.cannavigia.io/partner-api/1.0/documentation#/Authenticate

## Participant Store

Edit `public/participant-store.php` and

- Change the `$data` array to your needs.

Then open `participant-store.php` in your browser, e.g. http://example.org/participant-store.php.

### Documentation

https://cds.cannavigia.io/partner-api/1.0/documentation#/Participant/participantStore

## Participant Update

Edit `public/participant-update.php` and

- Change the `$data` array to your needs
- Change the `$participantId` to the participant ID you want to update

Then open `participant-update.php` in your browser, e.g. http://example.org/participant-update.php.

### Documentation

## Participant Update Status

Documentation: https://cds.cannavigia.io/partner-api/1.0/documentation#/Participant/participantParticipantUpdateStatus

Edit `public/participant-update-status.php` and

- Change the `$data` array to your needs
- Change the `$participantId` to the participant ID you want to update

Then open `participant-update-status.php` in your browser, e.g. http://example.org/participant-update-status.php.

### Documentation

https://cds.cannavigia.io/partner-api/1.0/documentation#/Participant/participantUpdate

## Packages Used

We used the following vendor packages for the implementation:

- https://github.com/vlucas/phpdotenv
- https://github.com/thephpleague/oauth2-client (https://oauth2-client.thephpleague.com/usage/)
- https://github.com/guzzle/guzzle
