<?php

require_once __DIR__ . '/../includes/bootstrap.php';
require_once __DIR__ . '/../includes/client.php';

use GuzzleHttp\Exception\RequestException;

$data = [
    // insert your refresh token here
    'refresh_token' => 'YOUR_REFRESH_TOKEN',
    'grant_type' => 'refresh_token',
    'client_id' => $_ENV['API_CLIENT_ID'],
    'client_secret' => $_ENV['API_CLIENT_SECRET'],
    'scope' => '',
];

try {
    $uri = '/partner-api/v1/token';

    $response = $client->request('POST', $uri, [
        'json' => $data,
    ]);

    $accessToken = json_decode($response->getBody()->getContents());

    // Save token in storage/session or somewhere
    $_SESSION['accessToken'] = $accessToken->access_token;
    $_SESSION['refreshToken'] = $accessToken->refresh_token;

    // We have an access token, which we may use in authenticated
    // requests against the service provider's API.
    echo 'Access Token: ' . $accessToken->access_token . "\n\n";
    echo 'Refresh Token: ' . $accessToken->refresh_token . "\n\n";
    echo 'Expires: ' . time() + $accessToken->expires_in . "\n\n";
} catch (RequestException $e) {
    if ($e->hasResponse()) {
        $errorMessage = $e->getResponse()->getBody()->getContents();
        echo "Error Message: {$errorMessage}\n";
    } else {
        echo $e->getMessage();
    }
}
