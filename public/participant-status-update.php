<?php

require_once __DIR__ . '/../includes/bootstrap.php';
require_once __DIR__ . '/../includes/client.php';

use GuzzleHttp\Exception\RequestException;

$data = [
    'status' => 'inactive', // replace with new status
    // 'reasons' => ['ZC_I01', 'ZC_I02'] // optional, replace with valid status reasons for your project
];

try {
    $participantID = 'XXXXXX'; // replace with participantID
    $uri = "/partner-api/v1/participant/{$participantID}/update-status";

    $response = $client->request('PUT', $uri, [
        'json' => $data,
    ]);

    $statusCode = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    echo "Status Code: {$statusCode}\n\n";
    echo "Response: {$content}\n\n";
} catch (RequestException $e) {
    if ($e->hasResponse()) {
        $errorMessage = $e->getResponse()->getBody()->getContents();
        echo "Error Message: {$errorMessage}\n";
    } else {
        echo $e->getMessage();
    }
}
