<?php

require_once __DIR__ . '/../includes/bootstrap.php';
require_once __DIR__ . '/../includes/client.php';

use GuzzleHttp\Exception\RequestException;

$data = [
    'hash' => '84f71e13b359b35ddedc07d9d58cc78fc0323c84edd44fafa1faee3446053ae7', // replace with participant hash
    'id_type' => 'SP', // replace with new ID type
    'id_number' => 'C9876543', // replace with new ID number
    'place_of_birth_or_residence' => 'Liestal BL', // replace with new place of birth or residence
];

try {
    $participantID = 'XXXXXX'; // replace with participantID
    $uri = "/partner-api/v1/participant/{$participantID}";

    $response = $client->request('PUT', $uri, [
        'json' => $data,
    ]);

    $statusCode = $response->getStatusCode();
    $content = $response->getBody()->getContents();

    echo "Status Code: {$statusCode}\n\n";
    echo "Response: {$content}\\n";
} catch (RequestException $e) {
    if ($e->hasResponse()) {
        $errorMessage = $e->getResponse()->getBody()->getContents();
        echo "Error Message: {$errorMessage}\n";
    } else {
        echo $e->getMessage();
    }
}
