<?php

require_once __DIR__ . '/../includes/bootstrap.php';
require_once __DIR__ . '/../includes/client.php';

use GuzzleHttp\Exception\ClientException;

$data = [
    'hash' => '84f71e13b359b35ddeeb07d9d58cc78fc0323c84edd44fafa1faee3446053ae7',
    'id_type' => 'SP',
    'id_number' => 'C9876543',
    'place_of_birth_or_residence' => 'Liestal BL',
    // 'dispensaries' => ['9012345000004', '9012345100001'], // Optional field
];

try {
    $response = $client->post('/partner-api/v1/participant', [
        'body' => json_encode($data),
    ]);

    $statusCode = $response->getStatusCode();

    $content = $response->getBody()->getContents();

    echo "Status Code: {$statusCode}\n\n";
    echo "Content: {$content}\n\n";
} catch (ClientException $e) {
    echo "Request failed. Error: {$e->getMessage()}\n";
}
