<?php

$provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => $_ENV['API_CLIENT_ID'],
    'clientSecret'            => $_ENV['API_CLIENT_SECRET'],
    'redirectUri'             => $_ENV['API_REDIRECT_URL'],
    'urlAuthorize'            => $_ENV['API_PREFIX'] . '/authorize',
    'urlAccessToken'          => $_ENV['API_PREFIX'] . '/partner-api/v1/token',
    'urlResourceOwnerDetails' => null,
]);
