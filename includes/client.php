<?php

use GuzzleHttp\Client;

$client = new Client([
    'base_uri' =>  $_ENV['API_PREFIX'],
    'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'Authorization' => 'Bearer ' . $_SESSION['accessToken'],
    ],
]);
