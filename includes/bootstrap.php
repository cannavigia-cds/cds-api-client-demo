<?php

require_once __DIR__ . '/../vendor/autoload.php';

session_start();

header('Content-type: text/plain');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();
